package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class chackWinDrawJUnitTest {
    
    public chackWinDrawJUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }


    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testChackWin_O_row1_output_ture() {
        char[][] board = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_X_row2_output_ture() {
        char[][] board = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_O_row3_output_ture() {
        char[][] board = {{'-','-','-'},{'X','X','O'},{'O','O','O'}};
        char CurrenPlayer = 'O';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_X_col1_output_ture() {
        char[][] board = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_O_col2_output_ture() {
        char[][] board = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        char CurrenPlayer = 'O';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_X_col3_output_ture() {
        char[][] board = {{'-','-','X'}, {'-','-','X'}, {'-','-','X'}};
        char CurrenPlayer = 'X';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_O_Digonal1_output_ture() {
        char[][] board = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', '-', 'O'}};
        char CurrenPlayer = 'O';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_X_Digonal2_output_ture() {
        char[][] board = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testChackWin_X_Draw_output_ture() {
        char[][] board = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', '-', '-'}};
        char CurrenPlayer = 'X';
        boolean result = Lab3.checkWin(board, CurrenPlayer);
        assertEquals(true, result);
    }
}
